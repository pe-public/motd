# Allows users to add extra text to /etc/motd by overriding the variable
# 'text' (which is used in the template).  Used as so:
#
#   class s_timeshare::motd inherits profile::base::os {
#       Motd["/etc/motd"] { text => template("foo/bar/motd.erb") }
#   }

define motd(
  $ensure,
  $template,
  $text = ''
) {
  file { $name:
    ensure  => $ensure,
    content => template($template),
  }
}
